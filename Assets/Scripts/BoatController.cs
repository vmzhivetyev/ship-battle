﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;

public class BoatController : NetworkBehaviour
{
    [SyncVar]
    public float health = 100;
    public float maxSpeed = 10;
    public float rotationSpeed = 2;
    public float acceleration = 1;
    public float sideDrag = 0.1f;
    public GameObject leftPaddle;
    public GameObject rightPaddle;
    public GameObject splashParticle;
    public GameObject trails;
    [HideInInspector]
    public float attacking = 0;
    public Text healthText;
    public LayerMask layers;

    void Start()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        UnityStandardAssets.Cameras.FreeLookCam.shared.SetTarget(transform);
    }

    private Vector3 smoothVelocity = Vector3.zero;
    void FixedUpdate()
    {
        if (attacking > 0)
        {
            attacking -= Time.fixedDeltaTime;
        }

        if (!isLocalPlayer)
            return;
        
        if (health <= 0)
        {
            foreach (var trail in trails.GetComponentsInChildren<ParticleSystem>())
                trail.Stop();

            if(UnityStandardAssets.Cameras.FreeLookCam.shared.Target)
                UnityStandardAssets.Cameras.FreeLookCam.shared.SetTarget(null);
            GetComponent<Rigidbody>().velocity = Vector3.SmoothDamp(GetComponent<Rigidbody>().velocity, Vector3.down * 10, ref smoothVelocity, 10);

            Vector3 rot = transform.rotation.eulerAngles;
            rot.x = -50;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(rot), 0.2f);

            if(transform.position.y < -50)
            {
                Destroy(gameObject);
            }

            return;
        }


        var r = transform.rotation.eulerAngles;
        r.x = 0;
        r.z = 0;
        transform.rotation = Quaternion.Euler(r);

        var p = transform.position;
        p.y = 0;
        transform.position = p;

        if (attacking > 0)
        {
            return;
        }

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float dt = Time.fixedDeltaTime;
        float mass = GetComponent<Rigidbody>().mass;
        Vector3 vel = GetComponent<Rigidbody>().velocity;

        GetComponent<Rigidbody>().AddTorque(Vector3.up * x * rotationSpeed * dt * mass);
        GetComponent<Rigidbody>().AddForce(transform.forward * y * acceleration * dt * mass);
        vel = Vector3.ClampMagnitude(vel, maxSpeed);
        vel = Vector3.Lerp(vel, Vector3.Project(vel, transform.forward), dt * sideDrag);
        vel.y = 0;

        GetComponent<Rigidbody>().velocity = vel;
    }

    [Command]
    public void CmdSendDamage(GameObject boat)
    {
        boat.GetComponent<BoatController>().RpcApplyDamage(UnityEngine.Random.Range(50f, 200f));
    }

    [ClientRpc]
    public void RpcApplyDamage(float d)
    {
        CmdApplyDamage(d);
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            healthText.transform.parent.gameObject.SetActive(false);

            return;
        }

        healthText.text = "❤ "+ (int)Mathf.Clamp(health + 1, 0, health);

        if (health <= 0)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            CmdSetTrigger(false);
            
        }

        if (Input.GetMouseButtonDown(1))
        {
            CmdSetTrigger(true);
        }
    }

    [Command]
    public void CmdSetTrigger(bool right)
    {
        attacking = 1.5f;
        RpcTrig(right);
        if (right)
            rightPaddle.GetComponent<Animator>().SetTrigger("attack");
        else
            leftPaddle.GetComponent<Animator>().SetTrigger("attack");
    }

    [ClientRpc]
    void RpcTrig(bool right)
    {
        if (right)
            rightPaddle.GetComponent<Animator>().SetTrigger("attack");
        else
            leftPaddle.GetComponent<Animator>().SetTrigger("attack");
    }

    [Command]
    public void CmdApplyDamage(float damage)
    {
        if (!isServer)
        {
            return;
        }

        health -= damage;
    }

    public void ApplyDamage(float damageValue)
    {
        CmdApplyDamage(damageValue);
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            Debug.Log("Death");
        }
    }
}
