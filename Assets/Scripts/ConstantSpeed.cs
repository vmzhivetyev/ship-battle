﻿using UnityEngine;
using System.Collections;

public class ConstantSpeed : MonoBehaviour
{
    public Vector3 vel;
    public bool resetPos = false;
    private Vector3 defPos;

    void Start()
    {
        defPos = transform.position;
    }

    void Update()
    {
        if(resetPos)
        {
            resetPos = false;
            transform.position = defPos;
        }
        GetComponent<Rigidbody>().velocity = vel;
    }
}
