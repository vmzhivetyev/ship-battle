﻿using UnityEngine;
using System.Collections;

public class CameraEffectsController : MonoBehaviour
{
    public float fadeTime = 4;

    private float vel = 0f;

    void Update()
    {
        var targ = UnityStandardAssets.Cameras.FreeLookCam.shared.Target ? 0 : 2.5f;
        var time = UnityStandardAssets.Cameras.FreeLookCam.shared.Target ? 1f : fadeTime;

        GetComponent<UnityStandardAssets.ImageEffects.BloomOptimized>().intensity = Mathf.SmoothDamp(GetComponent<UnityStandardAssets.ImageEffects.BloomOptimized>().intensity, targ, ref vel, time);
    }
}
