﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MeatScript : NetworkBehaviour
{
    public GameObject monster;

    void OnCollisionEnter(Collision col)
    {
        if(isLocalPlayer && col.collider.CompareTag("MeatEater"))
            Destroy(gameObject);
    }

    void FixedUpdate()
    {
        if(transform.position.y <= 0)
        {
            var p = transform.position;
            p.y = 0;
            transform.position = p;

            var vel = GetComponent<Rigidbody>().velocity;
            vel.y = 0;
            GetComponent<Rigidbody>().velocity = vel;

            enabled = false;

            GetComponent<Rigidbody>().isKinematic = true;

            var s = Instantiate(monster) as GameObject;
            s.GetComponent<MonsterController>().AttackAtPoint(transform.position);
            NetworkServer.Spawn(s);
        }
    }
}
