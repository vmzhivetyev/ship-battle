﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class EdgeTrigger : NetworkBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (!isServer)
            return;

        Debug.Log("Out of the zone! " + col.gameObject.name);

        var go = col.gameObject.transform.parent.gameObject;
        var boat = go.GetComponent<BoatController>();
        if (boat)
        {
            boat.ApplyDamage(boat.health);
        }
        else
        {
            Destroy(go);
        }
    }
}
