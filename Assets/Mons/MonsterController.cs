﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class MonsterController : NetworkBehaviour
{

    public float timeOfJump = 0.5f;

    private Rigidbody rig;
    private bool isCrossed = false;
    private Vector3 zeroPoint;
    private Vector3 target;

    void Awake()
    {
        rig = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        transform.rotation = Quaternion.LookRotation(rig.velocity);

        if (!isServer)
            return;

        if (!isCrossed)
        {
            if (transform.position.y >= 0)
            {
                isCrossed = true;
                var vy = -Physics.gravity.y * timeOfJump / 2;
                var vx = new Vector2(target.x - transform.position.x, target.z - transform.position.z) / timeOfJump;
                rig.velocity = new Vector3(vx.x, vy, vx.y);
            }
            else
            {
                rig.velocity = (zeroPoint - transform.position);
            }
        }
    }

    void Update()
    {
        if (!isServer)
            return;

        if (transform.position.y < -60)
            Destroy(gameObject);
    }

    public void AttackAtPoint(Vector3 point)
    {
        target = point;
        var offset = new Vector2(Random.Range(6f, 9f) * (Random.value > 0.5 ? 1 : -1), Random.Range(6f, 9f) * (Random.value > 0.5 ? 1 : -1));
        timeOfJump = offset.magnitude / 8f;
        zeroPoint = new Vector3(point.x - offset.x, 6, point.z - offset.y);
        transform.position = zeroPoint - new Vector3(offset.x, 9.81f * Mathf.Pow(timeOfJump, 2f) / 2f, offset.y);
        print(transform.position);
    }

    void OnTriggerEnter(Collider col)
    {
        if (!isServer)
            return;

        Debug.Log("Shark hit " + col.gameObject.name);
        if (col.gameObject.CompareTag("Meat"))
            Destroy(col.gameObject);

        CmdSendDamage(col.transform.root.gameObject);
    }

    [Command]
    void CmdSendDamage(GameObject boat)
    {
        var b = boat.GetComponent<BoatController>();
        if (b) b.RpcApplyDamage(Random.Range(200f, 300f));
    }
}
