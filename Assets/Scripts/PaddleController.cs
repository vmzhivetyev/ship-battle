﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PaddleController : MonoBehaviour
{
    public Transform velocityPoint;
    public Transform splashPoint;

    private BoatController boatController;
    private float speedForOneAnimSpeed = 2;
    private Vector3 prevPos;

    void Awake()
    {
        boatController = transform.root.GetComponent<BoatController>();
    }

    void Start()
    {
        prevPos = velocityPoint.position;
    }

    void Update()
    {
        var move = Vector3.Project((prevPos - velocityPoint.position), boatController.transform.forward);
        float speed = move.magnitude / Time.deltaTime / speedForOneAnimSpeed;
        var goingBack = Vector3.Angle(move, boatController.transform.forward) < 90;
        if (goingBack)
            speed *= -1;

        GetComponent<Animator>().SetFloat("speed", speed);

        prevPos = velocityPoint.position;
    }

    public void OnAttackMoment()
    {
        Instantiate(boatController.splashParticle, new Vector3(splashPoint.position.x, 0, splashPoint.position.z), Quaternion.identity);

        if (!transform.root.GetComponent<BoatController>().isLocalPlayer)
            return;

        var colls = Physics.OverlapSphere(splashPoint.position, 1, transform.root.GetComponent<BoatController>().layers);
        foreach (var c in colls)
        {
            if (c.transform.root != transform.root)
            {
                var boat = c.transform.root.gameObject;
                if (boat)
                {
                    boatController.CmdSendDamage(boat);
                }
            }
        }
    }
}
