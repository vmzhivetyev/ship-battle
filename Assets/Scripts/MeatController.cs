﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MeatController : NetworkBehaviour
{
    public GameObject meatPref;
    public Transform spawnPoint;
    public Animator legIK;
    public float cd = 0;

    void Start()
    {

    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (cd > 0)
            cd -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && cd <= 0)
        {
            cd = 4;
            var forw = Vector3.ProjectOnPlane(Camera.main.transform.forward, Vector3.up);
            CmdThrowMeat(spawnPoint.position, Quaternion.Euler(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)), forw);
            legIK.SetTrigger("attack");
        }
    }

    [Command]
    void CmdThrowMeat(Vector3 pos, Quaternion rot, Vector3 forw)
    {
        var meat = Instantiate(meatPref, pos, rot) as GameObject;

        
        if (forw.magnitude < 0.4)
        {
            Debug.Log(forw.magnitude);
            forw = forw.normalized * 0.4f;
        }
        meat.GetComponent<Rigidbody>().velocity = forw * Random.Range(5f, 7f) + Vector3.up * 10;

        NetworkServer.Spawn(meat);
    }
}
