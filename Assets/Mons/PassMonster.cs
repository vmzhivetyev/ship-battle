﻿using UnityEngine;
using System.Collections;

public class PassMonster : MonoBehaviour {

	private Rigidbody rig;
	private float time = 0;
	private Vector3 newVelocity;
	private Quaternion newRotation;

	void Start () 
	{
		rig = GetComponent<Rigidbody> ();
		rig.velocity = new Vector3 (Random.Range (2f, 4f), 0, Random.Range (2f, 4f));
		newVelocity = rig.velocity;
		transform.rotation = Quaternion.LookRotation (rig.velocity);
	}

	void FixedUpdate()
	{
		rig.velocity += Vector3.ClampMagnitude (newVelocity - rig.velocity, 0.04f);
		transform.rotation = Quaternion.LookRotation (rig.velocity);

		time -= Time.fixedDeltaTime;
		if (time <= 0) {
			newVelocity = Quaternion.Euler (0, Random.Range (16f, 32f) * (Random.value > 0.5 ? 1 : -1), 0) * rig.velocity;
			time = Random.Range (1f, 2f);
		}
	}

	void OnCollisionEnter(Collision collision) 
	{
		newVelocity = Quaternion.Euler (0, Random.Range (32f, 64f) * (Random.value > 0.5 ? 1 : -1), 0) * (rig.velocity * 1.5f);
	}
}
