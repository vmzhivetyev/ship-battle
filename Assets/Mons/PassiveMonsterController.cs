﻿using UnityEngine;
using System.Collections;

public class PassiveMonsterController : MonoBehaviour {

	private Rigidbody rig;
	private float time = 0;
	private Vector3 newVelocity;
	private Quaternion newRotation;

	void Start () 
	{
		rig = GetComponent<Rigidbody> ();
		rig.velocity = new Vector3 (Random.Range (2f, 4f), 0, Random.Range (2f, 4f));
		newVelocity = rig.velocity;
		transform.rotation = Quaternion.LookRotation (rig.velocity);
	}

	void FixedUpdate()
	{
		rig.velocity += Vector3.ClampMagnitude (newVelocity - rig.velocity, 0.08f);
		float angleDiff = Vector3.Angle (rig.velocity, newVelocity);
		if (angleDiff > 4) {
			Vector3 cross = Vector3.Cross(transform.forward, newVelocity.normalized);
			rig.AddTorque(cross/7f);
		}

		time -= Time.fixedDeltaTime;
		if (time <= 0) {
			newVelocity = Quaternion.Euler (0, Random.Range (32f, 64f) * (Random.value > 0.5 ? 1 : -1), 0) * rig.velocity;
			time = Random.Range (1.6f, 2f);
		}
	}

	void OnCollisionEnter(Collision collision) 
	{
		newVelocity = Quaternion.Euler (0, Random.Range (64f, 128f) * (Random.value > 0.5 ? 1 : -1), 0) * (rig.velocity * 1.5f);
	}
}
