﻿using UnityEngine;
using System.Collections;

public class IKController : MonoBehaviour
{
    public Transform leftHand;
    public Transform rightHand;
    public Transform leftFoot;
    public Transform rightFoot;

    void Start ()
    {
	}
	
	void OnAnimatorIK()
    {
        GetComponent<Animator>().SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
        GetComponent<Animator>().SetIKPositionWeight(AvatarIKGoal.RightHand, 1);

        GetComponent<Animator>().SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
        GetComponent<Animator>().SetIKRotationWeight(AvatarIKGoal.RightHand, 1);

        GetComponent<Animator>().SetIKPosition(AvatarIKGoal.LeftHand, leftHand.position);
        GetComponent<Animator>().SetIKPosition(AvatarIKGoal.RightHand, rightHand.position);

        GetComponent<Animator>().SetIKRotation(AvatarIKGoal.LeftHand, leftHand.rotation);
        GetComponent<Animator>().SetIKRotation(AvatarIKGoal.RightHand, rightHand.rotation);

        //legs

        GetComponent<Animator>().SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
        GetComponent<Animator>().SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);

        GetComponent<Animator>().SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1);
        GetComponent<Animator>().SetIKRotationWeight(AvatarIKGoal.RightFoot, 1);

        GetComponent<Animator>().SetIKPosition(AvatarIKGoal.LeftFoot, leftFoot.position);
        GetComponent<Animator>().SetIKPosition(AvatarIKGoal.RightFoot, rightFoot.position);

        GetComponent<Animator>().SetIKRotation(AvatarIKGoal.LeftFoot, leftFoot.rotation);
        GetComponent<Animator>().SetIKRotation(AvatarIKGoal.RightFoot, rightFoot.rotation);
    }
}
