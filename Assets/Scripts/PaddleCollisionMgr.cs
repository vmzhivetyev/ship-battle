﻿using UnityEngine;
using System.Collections;

public class PaddleCollisionMgr : MonoBehaviour
{
    public static float idleDamage = 0;
    public static float attackDamage = 50;
    
    void OnCollisionEnter(Collision col)
    {

        return;

        if (!transform.root.GetComponent<BoatController>().isLocalPlayer)
            return;

        var rootGO = col.collider.transform.root.gameObject; // attacker
        var bc = rootGO.GetComponent<BoatController>(); // attacker's BoatController script
        Debug.Log(bc.attacking + " " + bc.isLocalPlayer);
        if (rootGO != transform.root.gameObject && bc && bc.attacking > 0 && bc.isLocalPlayer)
        {
            transform.root.SendMessage("ApplyDamage", attackDamage, SendMessageOptions.DontRequireReceiver);
        }
    }
}
